package com.springh2.service;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.springh2.model.Book;
import com.springh2.repository.BookRepository;
//defining the business logic
@Service
public class BookService
{
@Autowired
BookRepository bookRepository;
//getting all student records
public List<Book> getAllBook()
{
List<Book> books = new ArrayList<Book>();
bookRepository.findAll().forEach(book -> books.add(book));
return books;
}
//getting a specific record
public Book getBookById(int id)
{
return bookRepository.findById(id).get();
}
public void saveOrUpdate(Book book)
{
    bookRepository.save(book);
}
//deleting a specific record
public void delete(int id) 
{
    bookRepository.deleteById(id);
}
}