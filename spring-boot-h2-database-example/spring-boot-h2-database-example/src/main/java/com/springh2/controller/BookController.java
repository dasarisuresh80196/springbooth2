package com.springh2.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.springh2.model.Book;
import com.springh2.service.BookService;
//creating RestController
@RestController
public class BookController
{
//autowired the BookService class
@Autowired
BookService bookService;
//creating a get mapping that retrieves all the books detail from the database
@GetMapping("/book")
private List<Book> getAllBook()
{
return bookService.getAllBook();
}
//creating a get mapping that retrieves the detail of a specific Book
@GetMapping("/book/{id}")
private Book getBook(@PathVariable("id") int id)
{
return bookService.getBookById(id);
}
//creating a delete mapping that deletes a specific Book
@DeleteMapping("/book/{id}")
private void deleteBook(@PathVariable("id") int id)
{
bookService.delete(id);
}
//creating post mapping that post the Book detail in the database
@PostMapping("/book")
private int saveBook(@RequestBody Book book)
{
bookService.saveOrUpdate(book);
return book.getId();
}
}
