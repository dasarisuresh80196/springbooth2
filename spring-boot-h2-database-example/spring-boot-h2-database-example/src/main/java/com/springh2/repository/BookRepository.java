package com.springh2.repository;
import org.springframework.data.repository.CrudRepository;
import com.springh2.model.Book;
public interface BookRepository extends CrudRepository<Book, Integer>
{
}
